terraform {
  required_providers {
    vsphere = {
      source  = "hashicorp/vsphere"
      version = "1.26.0"
    }
  }
}

variable "vc_user" {}
variable "vc_pass" {}
variable "vc_url" {}
variable "esxi_license_key" {}
variable "vcenter_license_key" {}
variable "esx_root_pass" {}
variable "vsphere_datacenter" {}
variable "management_cluster_name" {}
variable "compute_cluster_hosts" {}
variable "vds_name" {}
variable "vds_network_interfaces" {}
variable "vds_esx_mgmt_pg_name" {}
variable "vds_esx_mgmt_pg_vlan" {}
variable "vds_esx_vmo_pg_name" {}
variable "vds_esx_vmo_pg_vlan" {}
variable "vds2_name" {}
variable "vds2_network_interfaces" {}
variable "vmotion_ips" {}
variable "vmotion_netmask" {}
variable "vmotion_gateway" {}
variable "vds_vmware_app_pg_name" {}
variable "vds_vmware_app_pg_vlan" {}
variable "default_mgmt_nfs_datastore" {}
variable "default_mgmt_portgroup" {}
variable "default_ovf_deploy_host" {}

# VMware vSphere Provider
provider "vsphere" {
  user           = var.vc_user
  password       = var.vc_pass
  vsphere_server = var.vc_url

  # if you have a self-signed cert
  allow_unverified_ssl = true
}

resource "vsphere_license" "esxiLicenseKey" {
  license_key = var.esxi_license_key
}

resource "vsphere_license" "vcenterLicenseKey" {
  license_key = var.vcenter_license_key
}

resource "vsphere_datacenter" "datacenter" {
  name = var.vsphere_datacenter
}

resource "vsphere_compute_cluster" "compute_cluster" {
  name                 = var.management_cluster_name
  datacenter_id        = vsphere_datacenter.datacenter.moid
  drs_enabled          = true
  drs_automation_level = "fullyAutomated"
  ha_enabled           = true
  host_managed         = true
  ha_advanced_options = {
    "das.ignoreInsufficientHbDatastore" = "true"
    "das.ignoreRedundantNetWarning"     = "true"
  }
  ha_host_monitoring                              = "enabled"
  ha_host_isolation_response                      = "shutdown"
  ha_admission_control_policy                     = "resourcePercentage"
  ha_admission_control_resource_percentage_cpu    = 75
  ha_admission_control_resource_percentage_memory = 75
}

data "external" "compute_cluster_hosts" {
  count   = length(var.compute_cluster_hosts)
  program = ["pwsh", "-Command", "D:\\BitBucket\\ORE-US\\Terraform\\get-thumbprint.ps1 -ComputerName ${var.compute_cluster_hosts[count.index]}"]
}

resource "vsphere_host" "compute_cluster_hosts" {
  count      = length(var.compute_cluster_hosts)
  hostname   = var.compute_cluster_hosts[count.index]
  username   = "root"
  password   = var.esx_root_pass
  license    = var.esxi_license_key
  cluster    = vsphere_compute_cluster.compute_cluster.id
  thumbprint = data.external.compute_cluster_hosts[count.index].result.thumbprint
  force      = true
}

resource "vsphere_distributed_virtual_switch" "dvs" {
  name                   = var.vds_name
  datacenter_id          = vsphere_datacenter.datacenter.moid
  allow_forged_transmits = false
  allow_promiscuous      = false
  allow_mac_changes      = false
  max_mtu                = 9000
  uplinks                = ["uplink1", "uplink2"]
  active_uplinks         = ["uplink1", "uplink2"]
  standby_uplinks        = []
  dynamic "host" {
    for_each = vsphere_host.compute_cluster_hosts
    content {
      host_system_id = host.value["id"]
      devices        = var.vds_network_interfaces
    }
  }
}

resource "vsphere_distributed_port_group" "esx_mgmt" {
  name                            = var.vds_esx_mgmt_pg_name
  vlan_id                         = var.vds_esx_mgmt_pg_vlan
  distributed_virtual_switch_uuid = vsphere_distributed_virtual_switch.dvs.id
}

resource "vsphere_distributed_port_group" "esx_vmotion" {
  name                            = var.vds_esx_vmo_pg_name
  vlan_id                         = var.vds_esx_vmo_pg_vlan
  distributed_virtual_switch_uuid = vsphere_distributed_virtual_switch.dvs.id
}

resource "vsphere_distributed_port_group" "app_vcenter" {
  name                            = var.vds_vmware_app_pg_name
  vlan_id                         = var.vds_vmware_app_pg_vlan
  distributed_virtual_switch_uuid = vsphere_distributed_virtual_switch.dvs.id
}

resource "vsphere_vnic" "esx_vmotion" {
  count                   = length(var.compute_cluster_hosts)
  host                    = vsphere_host.compute_cluster_hosts[count.index].id
  distributed_switch_port = vsphere_distributed_virtual_switch.dvs.id
  distributed_port_group  = vsphere_distributed_port_group.esx_vmotion.id
  ipv4 {
    ip      = var.vmotion_ips[count.index]
    netmask = var.vmotion_netmask
    gw      = var.vmotion_gateway
  }
  mtu      = 9000
  netstack = "vmotion"
}

data "vsphere_host" "mgmt_host_1" {
  name = "ore21ecm001.ore-us.net"
  datacenter_id = vsphere_datacenter.datacenter.moid
}

data "vsphere_host" "mgmt_host_2" {
  name = "ore21ecm002.ore-us.net"
  datacenter_id = vsphere_datacenter.datacenter.moid
}

resource "vsphere_distributed_virtual_switch" "nsxt_vds" {
  name                   = var.vds2_name
  datacenter_id          = vsphere_datacenter.datacenter.moid
  allow_forged_transmits = false
  allow_promiscuous      = false
  allow_mac_changes      = false
  max_mtu                = 9000
  uplinks                = ["uplink1", "uplink2"]
  active_uplinks         = ["uplink1", "uplink2"]
  standby_uplinks        = []
  
  host {
    host_system_id = data.vsphere_host.mgmt_host_1.id
    devices = var.vds2_network_interfaces
  }

  host {
    host_system_id = data.vsphere_host.mgmt_host_2.id
    devices = var.vds2_network_interfaces
  }
}

data "vsphere_datastore" "ovf_datastore" {
  name          = var.default_mgmt_nfs_datastore
  datacenter_id = vsphere_datacenter.datacenter.moid
}

data "vsphere_resource_pool" "ovf_pool" {
  name          = "${vsphere_compute_cluster.compute_cluster.name}/Resources"
  datacenter_id = vsphere_datacenter.datacenter.moid
}

data "vsphere_host" "ovf_host" {
  name          = var.default_ovf_deploy_host
  datacenter_id = vsphere_datacenter.datacenter.moid
}


resource "vsphere_virtual_machine" "Nsxt_mgr_FromOvf" {
  name                       = "ORE21NSXT01"
  resource_pool_id           = data.vsphere_resource_pool.ovf_pool.id
  datastore_id               = data.vsphere_datastore.ovf_datastore.id
  host_system_id             = data.vsphere_host.ovf_host.id
  wait_for_guest_net_timeout = 0
  wait_for_guest_ip_timeout  = 0
  datacenter_id              = vsphere_datacenter.datacenter.moid
  num_cpus                   = 4
  num_cores_per_socket       = 1
  memory                     = 16384
  network_interface {
    network_id  =  vsphere_distributed_port_group.esx_mgmt.id
    ovf_mapping = vsphere_distributed_port_group.esx_mgmt.id
  }

  ovf_deploy {
    local_ovf_path    = "D:\\VirtualAppliances\\NSX-T-3.1.3.1.0\\nsx-unified-appliance-3.1.3.1.0.18504672.ovf"
    disk_provisioning = "thin"
    ip_protocol       = "IPv4"
    #ip_allocation_policy = "STATIC"
    deployment_option = "medium"

    ovf_network_map = {
      "Network 1" = vsphere_distributed_port_group.app_vcenter.id
    }
  }

  vapp {
    properties = {
      "nsx_passwd_0"           = "VMware1!VMware1!"
      "nsx_cli_passwd_0"       = "VMware1!VMware1!"
      "nsx_cli_audit_passwd_0" = "VMware1!VMware1!"
      "nsx_cli_username"       = "admin"
      "nsx_cli_audit_username" = "audit"
      "extraPara"              = " "
      "nsx_hostname"           = "ore21nsxt01"
      "nsx_role"               = "NSX Manager"
      "nsx_ip_0"               = "10.76.97.166"
      "nsx_netmask_0"          = "255.255.255.128"
      "nsx_gateway_0"          = "10.76.97.129"
      "nsx_dns1_0"             = "10.76.96.100 10.76.96.101"
      "nsx_domain_0"           = "ore-us.net"
      "nsx_ntp_0"              = "10.76.96.100 10.76.96.101"
      "nsx_isSSHEnabled"       = "True"
      "nsx_allowSSHRootLogin"  = "True"
      "nsx_swIntegrityCheck"   = "False"
      "mpIp"                   = " "
      "mpToken"                = " "
      "mpThumbprint"           = " "
      "mpNodeId"               = " "
      "mpClusterId"            = " "
    }
  }
  lifecycle {
    ignore_changes = [
      vapp,
      ovf_deploy
    ]
  }
}

######### DEPRECATED RESOURCES BELOW HERE ##############
# resource "vsphere_host_virtual_switch" "h1switch" {
#   name           = "vSwitch0"
#   host_system_id = "host-1030"
#   network_adapters = []
#   active_nics  = []
#   standby_nics = []
# }

# resource "vsphere_host_virtual_switch" "h2switch" {
#   name           = "vSwitch0"
#   host_system_id = "host-1029"
#   network_adapters = []
#   active_nics  = []
#   standby_nics = []
# }

# resource "vsphere_nas_datastore" "nfs_datastores" {
#   for_each        = { for ds in var.nfs_datastores : ds.nfs_share => ds }
#   name            = each.key
#   host_system_ids = data.vsphere_host.hosts.*.id
#   type            = "NFS"
#   remote_hosts    = [each.value.nfs_host]
#   remote_path     = "/${each.value.nfs_share}"
# }