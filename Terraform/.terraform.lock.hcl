# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/external" {
  version = "2.1.0"
  hashes = [
    "h1:X2gljqoAONRWjDeA+6eLIRt4pDS8QNRZZGhYIGS5Y14=",
    "zh:0d83ffb72fbd08986378204a7373d8c43b127049096eaf2765bfdd6b00ad9853",
    "zh:7577d6edc67b1e8c2cf62fe6501192df1231d74125d90e51d570d586d95269c5",
    "zh:9c669ded5d5affa4b2544952c4b6588dfed55260147d24ced02dca3a2829f328",
    "zh:a404d46f2831f90633947ab5d57e19dbfe35b3704104ba6ec80bcf50b058acfd",
    "zh:ae1caea1c936d459ceadf287bb5c5bd67b5e2a7819df6f5c4114b7305df7f822",
    "zh:afb4f805477694a4b9dde86b268d2c0821711c8aab1c6088f5f992228c4c06fb",
    "zh:b993b4a1de8a462643e78f4786789e44ce5064b332fee1cb0d6250ed085561b8",
    "zh:c84b2c13fa3ea2c0aa7291243006d560ce480a5591294b9001ce3742fc9c5791",
    "zh:c8966f69b7eccccb771704fd5335923692eccc9e0e90cb95d14538fe2e92a3b8",
    "zh:d5fe68850d449b811e633a300b114d0617df6d450305e8251643b4d143dc855b",
    "zh:ddebfd1e674ba336df09b1f27bbaa0e036c25b7a7087dc8081443f6e5954028b",
  ]
}

provider "registry.terraform.io/hashicorp/vsphere" {
  version     = "1.26.0"
  constraints = "1.26.0"
  hashes = [
    "h1:a0m4zSSaNrMM3ohnnKHvmldvp03tN4FqyMiwe1htVKk=",
    "zh:3a67a13d7cdb1209d5fa756e80b2f6a36f1db0df0d78b1b3e58a68a4834befdb",
    "zh:47f59fd678685fd5711990237272b27d45666ea209180baa0852e078dbc667ca",
    "zh:4a17e4d9b648b688e39982fdaa1310ec0e111dac3d2b84c2b39b8801f289a69d",
    "zh:5efa8a3fea7cf5b803dce07a2a5b8e78219e9ca2e7476ffd9fa7e5d2efcf7006",
    "zh:63fabd7423381094b3186388023ac24e5d03b1184e45d47cdd4165cfd11a1968",
    "zh:6bce88dd3401d121a45966aeb61f3015797389a7d0a6fdc84dad7c6e0705710a",
    "zh:92648675a4e8aa93d29a3d0a38c0bb278978ab6d40dd32713c1bfaad9632d34c",
    "zh:92a9457a284762c7f4aa7c7461ddb306b76284798f2b1ee6f7012d6a6d6934cd",
    "zh:ea031bf5138c09244df2ae43e942831afe7064f8d1973774ad8d7abcc5ef2936",
    "zh:f05a515fcae459a7ff99f92d288d6e14651f4edad28e2c6db023b159acaaed97",
    "zh:f09563dccd6dc2188bc315a32dfcc67aee20c668d026c5236b6a71f85d331951",
  ]
}
