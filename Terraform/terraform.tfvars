vc_user                 = "administrator@vsphere.local"
vc_pass                 = "VMware1!VMware1!"
vc_url                  = "ore21vcm001.ore-us.net"
esxi_license_key        = "1N293-8T25N-582G8-0CEUM-1J0MN"
vcenter_license_key     = "1H09P-09J0L-082G3-00C24-1XA2J"
esx_root_pass           = "VMware1!"
vsphere_datacenter      = "ORE (US1)"
management_cluster_name = "ORE21-MGMT"
compute_cluster_hosts   = ["ore21ecm001.ore-us.net", "ore21ecm002.ore-us.net"]
vds_name                = "ORE21-MGMT-VDS"
vds_network_interfaces  = ["vmnic0", "vmnic1"]
vds_esx_mgmt_pg_name    = "esx_mgmt_15"
vds_esx_mgmt_pg_vlan    = 15
vds_esx_vmo_pg_name     = "esx_vmotion_16"
vds_esx_vmo_pg_vlan     = 16
vds2_name               = "ORE21-NSXT-VDS"
vds2_network_interfaces = ["vmnic4", "vmnic5"]
vmotion_ips             = ["10.76.98.21", "10.76.98.22"]
vmotion_netmask         = "255.255.255.0"
vmotion_gateway         = "10.76.98.1"
vds_vmware_app_pg_name  = "vmware_app_13"
vds_vmware_app_pg_vlan  = 13
default_mgmt_nfs_datastore = "ORE_MGMT_NFS01"
default_mgmt_portgroup = "vmware_app_13"
default_ovf_deploy_host = "ore21ecm001.ore-us.net"

# nfs_datastores = [
#   {
#     nfs_host  = "winsrv01.ecplab.local"
#     nfs_share = "esxdatastore"
#   },
#   {
#     nfs_host  = "esxnfs.ecplab.local"
#     nfs_share = "datastore01"
#   }
# ]